## Objectives

1. Students will get an introduction to the Sinatra framework
2. Students will gain experience in project planning and design
3. Using a provided Sinatra file structure, students will create simple web pages with some interactivity (forms)

## Resources

### Sinatra

* [Intro into Sinatra](https://gist.github.com/ashrithr/6460552)
* [What is Sinatra?](https://github.com/learn-co-curriculum/what-is-sinatra)
* [Using ERB](https://github.com/learn-co-curriculum/sinatra-using-erb)
* [Sinatra Documentation](http://sinatrarb.com/documentation.html)

### Project Planning/Wireframing

* [Gliffy - Free Wireframing Site](https://go.gliffy.com/go/html5/launch)
* [Draw.io - Another Free Wireframing Site](https://www.draw.io/)

### Sinatra and HTML Forms

* [HTML Forms and Params using Sinatra](https://github.com/learn-co-curriculum/sinatra-forms-params-readme-walkthrough)
* [HTML Forms and Ruby](https://learnrubythehardway.org/book/ex51.html)

### Application Programming Interfaces

* [Using APIs with Sinatra](https://github.com/learn-co-curriculum/sinatra-using-apis)
